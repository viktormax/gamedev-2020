# CodeOn Game

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs all required packages to run the application.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### How to create map

Rules to create map:
1. Map size 40x30
2. Lower World is under Player
3. World is on the same level with Player
    Player can't move over objects on this layer
4. Upper Layer is rendered above Player
5. Objects are rendered relatively to Player depending
    on it's order. Player can interact with objects.
6. Every object is image type. If object size is bigger, then
    image should be empty and resized as needed.

***LEVEL1***

function repeatMove(x) {
    for (let i = 0; i < x; i++) {
        move();
    }
}

function stairs(dir) {
    let f = dir == "up"? true : false;
    for (let i = 0; i < 5; i++) {
        repeatMove(3);
        if (f) {
            turnRight();
        } else {
            turnLeft();
        }
        f = !f;
    }
}

function outflank(n) {
    for (let i = 0; i < n; i++) {
        turnRight();
        move();
        turnLeft();
        repeatMove(2);
        turnLeft();
        move();
        turnRight();
        repeatMove(4);
    }
}

stairs("up");
take();
turnRight();
stairs("down");
repeatMove(6);
turnLeft();
stairs("up");
take();
turnRight();
stairs("down");

use();
take();
turnLeft();
repeatMove(7);
use();
repeatMove(7);
turnLeft();
repeatMove(6);
outflank(4);
move();
turnLeft();
repeatMove(6);
take();
turnRight();
move();
turnRight();
repeatMove(7);
use();

***END OF LEVEL1***