import Game from 'Pages/Game';
import { Route, createBrowserRouter, createRoutesFromElements } from 'react-router-dom';
import MenuLayout from 'Layout/MenuLayout';
import Levels from 'Pages/Levles';
import RoutePath from 'Enums/RoutePath';
import GameRouteFilter from 'Pages/Game/GameRouteFilter';
import ForBeginners from 'Pages/ForBeginners';
import HowToPlay from 'Pages/HowToPlay';
import About from 'Pages/About';
import Prologue from 'Pages/Prologue';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route>
      <Route element={<GameRouteFilter />}>
        <Route path={RoutePath.Game} element={<Game />} />
      </Route>
      <Route element={<MenuLayout />}>
        <Route path={RoutePath.HowToPlay} element={<HowToPlay />} />
        <Route path={RoutePath.Prologue} element={<Prologue />} />
        <Route path={RoutePath.Levels} element={<Levels />} />
        <Route path={RoutePath.ForBeginers} element={<ForBeginners />} />
        <Route path={RoutePath.About} element={<About />} />
        <Route path="*" />
      </Route>
    </Route>
  ),
  { basename: process.env.PUBLIC_URL }
);

export default router;
