enum StorageKeys {
  Acknowledged = 'acknowledged',
  LevelCode = 'levelCode',
  FinishedLevels = 'finishedLevels',
  Scores = 'scores',
  Language = 'language',
}

const replacer = (key: string, value: any) => {
  if (value instanceof Map) {
    return {
      dataType: 'Map',
      value: Array.from(value.entries()),
    };
  }
  return value;
};

const reviver = (key: string, value: any) => {
  if (typeof value === 'object' && value !== null) {
    if (value.dataType === 'Map') {
      return new Map(value.value);
    }
  }
  return value;
};

const createProperty = <T>(key: StorageKeys) => ({
  key: () => key,
  get: (index?: number): T | null => {
    const item = window.localStorage.getItem(index ? `${key}-${index}` : key);
    return item ? (JSON.parse(item, reviver) as T) : null;
  },
  set: (value: T, index?: number) =>
    window.localStorage.setItem(index ? `${key}-${index}` : key, JSON.stringify(value, replacer)),
  remove: (index?: number) => window.localStorage.removeItem(index ? `${key}-${index}` : key),
});

export type Language = 'sk' | 'en';

const localAppStorage = {
  acknowledged: createProperty<boolean>(StorageKeys.Acknowledged),
  levelCode: createProperty<string>(StorageKeys.LevelCode),
  finishedLevels: createProperty<number>(StorageKeys.FinishedLevels),
  scores: createProperty<Map<number, number>>(StorageKeys.Scores),
  language: createProperty<Language>(StorageKeys.Language),
};

export default localAppStorage;
