import ObjectData from 'Gameplay/ObjectData';

export const pathBuilder = (path: string) => {
  let query = path;

  const builder = {
    set: (parameterName: string, value: string | number | undefined | null) => {
      query = query.replace(`:${parameterName}`, String(value));
      return builder;
    },
    get: () => query.replaceAll(/:[a-z0-9]*\?/g, ''),
  };

  return builder;
};

export const generateSequance = (count: number, start: number = 0) =>
  Array(count)
    .fill(start)
    .map((element, index) => element + index);

export const getProperty = <T>(data: ObjectData, key: string, defaultValue: T): T =>
  data.properties?.find((p) => p.name === key)?.value ?? defaultValue;

export const getRandomIntInRange = (min: number, max: number): number =>
  Math.floor(Math.random() * (max - min) + min);
