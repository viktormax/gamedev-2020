import Door from 'Gameplay/Door';
import { Interactable, Takable, Usable } from 'Gameplay/Interfaces';

export const isDoor = (x: any): x is Door => x instanceof Door;

export const isTakable = (x: any): x is Takable => typeof x.take === 'function';

export const isUsable = (x: any): x is Usable => typeof x.use === 'function';

export const isInteractable = (x: any): x is Interactable => typeof x.canInteract === 'function';
