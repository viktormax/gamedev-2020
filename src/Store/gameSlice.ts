import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type GameState = 'idle' | 'play' | 'win' | 'lost' | 'pause' | 'restart';

interface State {
  state: GameState;
  energy: number;
  maxEnergy: number;
  totalCoins?: number;
  keys: number;
  coins: number;
  loading: boolean;
  debug: boolean;
}

const initialState: State = {
  state: 'idle',
  energy: 0,
  maxEnergy: 0,
  totalCoins: undefined,
  keys: 0,
  coins: 0,
  loading: true,
  debug: window.location.pathname.includes('debug'),
};

const gameSlice = createSlice({
  name: 'game',
  initialState,
  reducers: {
    setGameState: (state, action: PayloadAction<State['state']>) => {
      state.state = action.payload;
    },
    setTotalCoins: (state, action: PayloadAction<State['totalCoins']>) => {
      state.totalCoins = action.payload;
    },
    setCoins: (state, action: PayloadAction<State['coins']>) => {
      state.coins = action.payload;
    },
    setEnergy: (state, action: PayloadAction<State['energy']>) => {
      if (state.energy === 0) {
        state.state = 'lost';
      }

      state.energy = action.payload;
    },
    setKeys: (state, action: PayloadAction<State['keys']>) => {
      state.keys = action.payload;
    },
    setLoading: (state, action: PayloadAction<State['loading']>) => {
      state.loading = action.payload;
    },
    initPlayer: (state, action: PayloadAction<State['energy']>) => {
      state.energy = action.payload;
      state.maxEnergy = action.payload;
      state.coins = 0;
      state.keys = 0;
    },
    updateDebug: (state, action: PayloadAction<State['debug']>) => {
      state.debug = action.payload;
    },
  },
});

export const { setTotalCoins, setGameState, setCoins, initPlayer, setKeys, setEnergy, setLoading, updateDebug } =
  gameSlice.actions;
export default gameSlice;
