import { combineReducers, configureStore } from '@reduxjs/toolkit';
// eslint-disable-next-line no-restricted-imports
import { TypedUseSelectorHook, UseDispatch, useDispatch, useSelector } from 'react-redux';
import { enableMapSet } from 'immer';
import editorSlice from './editorSlice';
import localStorageSlice from './localStorageSlice';
import gameSlice from './gameSlice';

enableMapSet();

const reducer = combineReducers({
  editor: editorSlice.reducer,
  localStorage: localStorageSlice.reducer,
  game: gameSlice.reducer,
});

const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export const useAppDispatch: UseDispatch<AppDispatch> = useDispatch;

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export default store;
