import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface Editor {
  getValue: () => string;
}

interface State {
  editor: Editor | null;
}

const initialState: State = {
  editor: null,
};

const editorSlice = createSlice({
  name: 'editor',
  initialState,
  reducers: {
    setEditor: (state, action: PayloadAction<State['editor']>) => {
      state.editor = action.payload;
    },
  },
});

export const { setEditor } = editorSlice.actions;
export default editorSlice;
