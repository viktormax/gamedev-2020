import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localAppStorage, { Language } from 'Helpers/localAppStorage';
import i18n from 'i18n';

interface State {
  acknowledged: boolean;
  finishedLevels: number;
  language: Language;
  scores: Map<number, number>;
}

const initialState: State = {
  acknowledged: localAppStorage.acknowledged.get() ?? false,
  finishedLevels: localAppStorage.finishedLevels.get() ?? 0,
  scores: localAppStorage.scores.get() ?? new Map(),
  language: localAppStorage.language.get() ?? 'en',
};

i18n.changeLanguage(initialState.language);

const localStorageSlice = createSlice({
  name: 'localStorage',
  initialState,
  reducers: {
    setAcknolaged: (state, action: PayloadAction<State['acknowledged']>) => {
      state.acknowledged = action.payload;
      localAppStorage.acknowledged.set(action.payload);
    },
    setFinishedLevels: (state, action: PayloadAction<State['finishedLevels']>) => {
      state.finishedLevels = action.payload;
      localAppStorage.finishedLevels.set(action.payload);
    },
    setLanguage: (state, action: PayloadAction<State['language']>) => {
      state.language = action.payload;
      localAppStorage.language.set(action.payload);
    },
    setScore: (state, action: PayloadAction<{ level: number; score: number }>) => {
      state.scores.set(action.payload.level, action.payload.score);
      localAppStorage.scores.set(state.scores);
    },
  },
});

export const { setAcknolaged, setFinishedLevels, setScore, setLanguage } =
  localStorageSlice.actions;
export default localStorageSlice;
