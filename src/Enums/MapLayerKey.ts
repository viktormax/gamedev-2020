enum MapLayerKey {
  Upper = 'Upper World',
  Middle = 'World',
  Lower = 'Lower World',
  Objects = 'Objects',
}

export default MapLayerKey;
