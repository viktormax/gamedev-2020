enum TextureKey {
  Tiles = 'tiles',
  GoldKey = 'gold_key',
  ChestOpened = 'chest_opened',
  ChestClosed = 'chest_closed',
  DoorOpened = 'door_opened',
  DoorClosed = 'door_closed',
  HashOpened = 'hash_opened',
  HashClosed = 'hash_closed',
  ChestEmpty = 'chest_empty',
  LeverLeft = 'lever_left',
  LeverRight = 'lever_right',
  Skeleton = 'skeleton',
  Player = 'player',
  Bone = 'bone',
  Idle = 'idle',
  Run = 'run',
  TorchOn = 'torch_on',
  Fire = 'fire',
  Spikes = 'spikes',
}

export default TextureKey;
