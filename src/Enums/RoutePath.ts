enum RoutePath {
  Game = '/levels/:level/game/:debug?',
  Introduction = '/introduction',
  About = '/about',
  HowToPlay = '/how-to-play',
  Prologue = '/prologue',
  ForBeginers = '/for-beginners',
  Levels = '/levels',
  Root = '/',
}

export default RoutePath;
