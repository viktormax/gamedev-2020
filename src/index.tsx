import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import GlobalStyle from 'Theme/GlobalStyle';
import { Provider } from 'react-redux';
import store from 'Store';
import 'i18n';

const container = document.getElementById('root');

if (container) {
  ReactDOM.createRoot(container).render(
    <React.StrictMode>
      <Provider store={store}>
        <GlobalStyle />
        <App />
      </Provider>
    </React.StrictMode>
  );
}

serviceWorkerRegistration.register();
