import { Outlet } from 'react-router-dom';
import Icon from 'Components/Icon';
import RoutePath from 'Enums/RoutePath';
import castle from './castle.jpg';
import { ContentWrapper, LogoLink, MenuBar, MenuItem, MenuLayoutRoot, MenuLink } from './styles';
import { useTranslation } from 'react-i18next';
import Toggle, { ToggleOption } from 'Components/Toggle';
import { useEffect } from 'react';
import i18n from 'i18n';
import { useAppDispatch, useAppSelector } from 'Store';
import { setLanguage } from 'Store/localStorageSlice';
import useEvent from 'Hooks/useEvent';
import { Language } from 'Helpers/localAppStorage';

const toggleOptions: [ToggleOption, ToggleOption] = [
  {
    lable: 'EN',
    value: 'en',
  },
  {
    lable: 'SK',
    value: 'sk',
  },
];

const MenuLayout = () => {
  const { t } = useTranslation('layout');
  const language = useAppSelector((store) => store.localStorage.language);
  const dispatch = useAppDispatch();

  useEffect(() => {
    i18n.changeLanguage(language);
  }, [language]);

  const changeLanguage = useEvent((len: Language) => {
    dispatch(setLanguage(len));
  });

  return (
    <MenuLayoutRoot>
      <MenuBar>
        <LogoLink to={RoutePath.Root}>CODE/ON</LogoLink>
        <MenuItem>
          <MenuLink to={RoutePath.Levels}>
            {t('New Game')}
            <Icon icon="arrow" />
          </MenuLink>
        </MenuItem>
        <MenuItem>
          <MenuLink to={RoutePath.Prologue}>
            {t('Story')}
            <Icon icon="arrow" />
          </MenuLink>
        </MenuItem>
        <MenuItem>
          <MenuLink to={RoutePath.ForBeginers}>
            {t('For Beginners')}
            <Icon icon="arrow" />
          </MenuLink>
        </MenuItem>
        <MenuItem>
          <MenuLink to={RoutePath.HowToPlay}>
            {t('How To Play')}
            <Icon icon="arrow" />
          </MenuLink>
        </MenuItem>
        <MenuItem>
          <MenuLink to={RoutePath.About}>
            {t('About Game')}
            <Icon icon="arrow" />
          </MenuLink>
        </MenuItem>
        <Toggle options={toggleOptions} state={language} setState={changeLanguage} />
      </MenuBar>
      <ContentWrapper $image={castle}>
        <Outlet />
      </ContentWrapper>
    </MenuLayoutRoot>
  );
};

export default MenuLayout;
