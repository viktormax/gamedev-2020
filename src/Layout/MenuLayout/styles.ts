import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const MenuLayoutRoot = styled.div`
  min-height: 100%;
  display: flex;
  flex-wrap: nowrap;

  @media (max-width: 992px) {
    font-size: 90%;
  }

  @media (max-width: 720px) {
    font-size: 80%;
  }

  @media (max-width: 580px) {
    font-size: 60%;
  }
`;

export const MenuBar = styled.div`
  display: inline-block;
  background: #ffc50d;
  position: fixed;
  height: 100vh;
  width: 20em;
  z-index: 1;
`;

export const LogoLink = styled(Link)`
  all: unset;
  text-align: center;
  font-family: Roboto, sans-serif;
  font-size: 2.7em;
  display: block;
  margin: 1em 0.5em 2em 0.5em;
  color: black;
  letter-spacing: 0.02em;
  word-spacing: -0.15em;
  cursor: pointer;
  user-select: none;

  &:after {
    content: '>';
  }

  &:before {
    content: '<';
  }
`;

export const MenuItem = styled.div`
  position: relative;
  padding: 0 1em;
  background: #ffd44a;
  margin-top: 0.3em;
  cursor: pointer;

  &:hover {
    background: white;
    color: #282c34;
  }
`;

export const ContentWrapper = styled.div<{ $image: string }>`
  margin-left: 20em;
  width: calc(100vw - 20em);
  height: 100vh;
  z-index: 0;

  &::before {
    content: '';
    position: fixed;
    width: 100vw;
    height: 100vh;
    left: 0;
    background: url(${(p) => p.$image}) top no-repeat;
    background-size: cover;
    z-index: -1;
  }
`;

export const MenuLink = styled(Link)`
  display: block;
  padding: 1em 0;
  text-transform: uppercase;
  text-align: left;
  font-family: Roboto, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 1.5em;
  line-height: 1.2em;
  color: black;
  text-decoration: none;
  transition: all 0.2s linear;

  svg {
    width: 18px;
    height: auto;
    position: absolute;
    top: 50%;
    right: 10px;
    transform: translateY(-50%) translateX(-50%);
  }
`;
