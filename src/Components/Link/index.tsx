import React, { memo } from 'react';
import { LinkRoot } from './styles';

interface Props {
  href: string;
  children: React.ReactNode;
  margin?: string;
}

const Link: React.FC<Props> = ({ children, href, margin }) => (
  <LinkRoot href={href} style={{ margin }}>
    {children}
  </LinkRoot>
);

export default memo(Link);
