import styled from 'styled-components';

export const LinkRoot = styled.a`
  color: #1f8efa;
  text-decoration: none;
  font-weight: 500;
  font-size: 1.1em;
`;
