import { FlexCenter } from 'Theme/common';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const HeaderRoot = styled.div`
  height: 64px;
  background: #293241;
  position: relative;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-shrink: 0;
  padding: 0 24px;
`;

export const EnergyRoot = styled.div`
  height: 42px;
  display: flex;
  align-items: center;

  svg {
    z-index: 100;
    height: 42px;
    width: 42px;
    background: #44a5ff;
    border-radius: 50%;
  }
`;

export const EnergyBar = styled.div`
  margin-left: -3px;
  height: 20px;
  width: 20vw;
  background: rgba(255, 255, 255, 0.35);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 0px 8px 8px 0px;
`;

export const EnergyBarFill = styled.div`
  height: 20px;
  background: #6db9ff;
  border-radius: 0px 8px 8px 0px;
`;

export const OffButton = styled(Link)`
  border-radius: 100%;
  cursor: pointer;
  display: flex;
`;

export const PlayButton = styled(FlexCenter)`
  background: #ff6f00;
  color: white;
  height: 42px;
  width: 42px;
  border-radius: 50%;
  cursor: pointer;
`;
