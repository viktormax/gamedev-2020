import Icon from 'Components/Icon';
import { EnergyBar, EnergyBarFill, EnergyRoot } from './styles';
import { memo } from 'react';

interface Props {
  maxEnergy: number;
  energy: number;
}

const Energy: React.FC<Props> = ({ energy, maxEnergy }) => (
  <EnergyRoot title={`${((energy * 100) / maxEnergy).toFixed(1)}%`}>
    <Icon icon="thunder" />
    <EnergyBar>
      <EnergyBarFill style={{ width: `${(energy * 100) / maxEnergy}%` }} />
    </EnergyBar>
  </EnergyRoot>
);

export default memo(Energy);
