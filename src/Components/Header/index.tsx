import React, { memo } from 'react';
import Item from 'Components/Item';
import Energy from './Energy';
import RoutePath from 'Enums/RoutePath';
import { useAppDispatch, useAppSelector } from 'Store';
import { HeaderRoot, OffButton, PlayButton } from './styles';
import Icon from 'Components/Icon';
import { useTranslation } from 'react-i18next';
import { Row } from 'Theme/common';
import { setGameState } from 'Store/gameSlice';
import useEvent from 'Hooks/useEvent';

interface Props {
  level: number;
}

const Header: React.FC<Props> = ({ level }) => {
  const game = useAppSelector((store) => store.game);
  const scores = useAppSelector((store) => store.localStorage.scores);
  const { state, coins, totalCoins, energy, maxEnergy, keys, debug } = game;
  const dispatch = useAppDispatch();
  const { t } = useTranslation();
  const canRestart = state === 'win' || state === 'lost' || state === 'play' || debug;
  const score = scores.get(level);

  const toggleGameState = useEvent(() => {
    if (canRestart) {
      dispatch(setGameState('restart'));
    } else if (state === 'idle') {
      dispatch(setGameState('play'));
    }
  });

  return (
    <HeaderRoot>
      <Row $gap="24px">
        <Energy energy={energy} maxEnergy={maxEnergy} />
        <Item image="key" count={keys} />
        <Item image="gold" count={coins} total={totalCoins} />
      </Row>
      <PlayButton onClick={toggleGameState}>
        <Icon icon={canRestart ? 'restart' : 'play'} size={36} />
      </PlayButton>
      <Row $gap="48px">
        {score && <Item text={`${t('Score')}: ${score}`.toUpperCase()} />}
        <Item text={`${t('Level')} ${level + 1}`.toUpperCase()} />
        <OffButton to={RoutePath.Levels} title={t('Exit')}>
          <Icon icon="off" size={40} />
        </OffButton>
      </Row>
    </HeaderRoot>
  );
};

export default memo(Header);
