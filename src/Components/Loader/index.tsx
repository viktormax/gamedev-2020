import { memo } from 'react';
import { Line, LineWrapper, LoaderRoot, LoaderWrapper } from './styles';

const Loader = () => (
  <LoaderRoot>
    <LoaderWrapper>
      <LineWrapper>
        <Line />
      </LineWrapper>
      <LineWrapper>
        <Line />
      </LineWrapper>
      <LineWrapper>
        <Line />
      </LineWrapper>
      <LineWrapper>
        <Line />
      </LineWrapper>
      <LineWrapper>
        <Line />
      </LineWrapper>
    </LoaderWrapper>
  </LoaderRoot>
);

export default memo(Loader);
