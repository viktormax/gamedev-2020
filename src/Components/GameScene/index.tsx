import React, { useEffect, useRef } from 'react';
import Phaser from 'phaser';
import Scene from 'Gameplay/Scene';
import Loader from 'Components/Loader';
import WinMessage from 'Components/WinMessage';
import { useAppDispatch, useAppSelector } from 'Store';
import getConfig from 'Components/GameScene/getConfig';
import { LEVELS } from 'Pages/Levles';
import { GameSceneRoot } from './styles';
import { setGameState } from 'Store/gameSlice';
import LostMessage from 'Components/LostMessage';
import { setFinishedLevels, setScore } from 'Store/localStorageSlice';
import { minify } from 'terser';
import useEvent from 'Hooks/useEvent';
import declarations from 'Components/Editor/declarations';

interface Props {
  width: string;
  level: number;
}

const GameScene: React.FC<Props> = ({ width, level }) => {
  const gameRef = useRef<Phaser.Game>();
  const sceneRef = useRef<Scene>();
  const game = useAppSelector((store) => store.game);
  const editor = useAppSelector((store) => store.editor.editor);
  const scores = useAppSelector((store) => store.localStorage.scores);
  const dispatch = useAppDispatch();

  const updateScore = useEvent(async () => {
    const globalCode = declarations.replaceAll('declare', '').replaceAll(': void', '{}');
    const config = {
      mangle: {
        toplevel: true,
      },
      nameCache: {},
    };

    await minify(globalCode, config);
    const minified = await minify(editor?.getValue() ?? '', config);

    // eslint-disable-next-line no-console
    console.log('Minified code:', minified.code);

    const energyToCodeRatio = (game.maxEnergy / (minified.code?.length ?? Infinity)) * 100;
    const score = Math.round(energyToCodeRatio + game.energy);
    dispatch(setScore({ level, score }));
  });

  useEffect(() => {
    const scene = sceneRef.current;
    if (!scene) {
      return;
    }

    if (game.state === 'restart') {
      scene.scene.restart();
      dispatch(setGameState('idle'));
    }

    if (game.state === 'play') {
      scene.sheduleCode(editor?.getValue());
    }

    if (game.state === 'win') {
      dispatch(setFinishedLevels(level + 1));
      updateScore();
    }
  }, [dispatch, editor, game.state, level, updateScore]);

  useEffect(() => {
    dispatch(setGameState('idle'));
    if (!sceneRef.current) {
      sceneRef.current = new Scene(level);
      gameRef.current = new Phaser.Game(getConfig(sceneRef.current, game.debug));
    }

    return () => {
      dispatch(setGameState('idle'));
      gameRef.current?.destroy(true);
      sceneRef.current = undefined;
      gameRef.current = undefined;
    };
  }, [dispatch, game.debug, level]);

  return (
    <GameSceneRoot $width={width}>
      {game.state === 'win' && (
        <WinMessage score={scores.get(level)} nextLevel={LEVELS[level + 1]} />
      )}
      {game.state === 'lost' && <LostMessage />}
      {game.loading && <Loader />}
      <div id="game" />
    </GameSceneRoot>
  );
};

export default GameScene;
