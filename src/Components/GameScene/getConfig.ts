import Phaser from 'phaser';

const getConfig = (scene: Phaser.Scene, debug: boolean): Phaser.Types.Core.GameConfig => ({
  type: Phaser.AUTO,
  parent: 'game',
  width: 2100,
  height: 1440,
  backgroundColor: '0x282c34',
  scale: {
    zoom: Phaser.Scale.MAX_ZOOM,
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  physics: {
    default: 'matter',
    matter: {
      debug,
      gravity: { y: 0 },
    },
  },
  scene
});

export default getConfig;
