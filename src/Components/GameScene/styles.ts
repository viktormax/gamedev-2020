import styled from 'styled-components';

export const GameSceneRoot = styled.div<{ $width: string }>`
  width: ${(p) => p.$width};
  height: calc(100vh - 64px);
  background: #282c34;
  position: relative;

  canvas {
    height: 100%;
    width: 100%;
  }
`;
