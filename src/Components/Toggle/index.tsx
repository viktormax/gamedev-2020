import React, { memo } from 'react';
import { Circle, ToggleRoot } from './styles';

export type ToggleOption = {
  lable: string;
  value: any;
};

interface Props {
  options: [ToggleOption, ToggleOption];
  state: ToggleOption['value'];
  setState: (value: ToggleOption['value']) => void;
}

const Toggle: React.FC<Props> = ({ options, state, setState }) => {
  const isLeft = state === options[0].value;

  const toggle = () => {
    setState(isLeft ? options[1].value : options[0].value);
  };

  return (
    <ToggleRoot onClick={toggle}>
      <Circle $left={isLeft}>{isLeft ? options[0].lable : options[1].lable}</Circle>
    </ToggleRoot>
  );
};

export default memo(Toggle);
