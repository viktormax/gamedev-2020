import styled from 'styled-components';

export const ToggleRoot = styled.div`
  cursor: pointer;
  position: absolute;
  width: 100px;
  height: 50px;
  bottom: 3vh;
  left: 50%;
  transform: translateX(-50%);
  background: #ffffff;
  border-radius: 1000px;
`;

export const Circle = styled.div<{ $left: boolean }>`
  left: ${(p) => (p.$left ? '5px' : 'calc(100% - 45px)')};
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  border-radius: 100%;
  top: 5px;
  right: 0;
  width: 40px;
  height: 40px;
  background: #ffc50d;
  font-family: Roboto, sans-serif;
  font-weight: bold;
  text-transform: uppercase;
  transition: all 0.15s linear;
`;
