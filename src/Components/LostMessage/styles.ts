import styled from 'styled-components';

export const LostMessageRoot = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 99999;
  background: rgba(40, 44, 52, 0.6);
  position: absolute;
  flex-direction: column;

  h1 {
    margin: 20px;
    font-size: 4em;
    color: #ff5500;
  }

  h2 {
    margin: 0;
    font-size: 1.6em;
    color: white;
  }
`;
