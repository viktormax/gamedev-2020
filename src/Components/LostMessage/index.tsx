import { useTranslation } from 'react-i18next';
import { LostMessageRoot } from './styles';

const LostMessage = () => {
  const { t } = useTranslation('common');

  return (
    <LostMessageRoot>
      <h1>{t('You lost!')}</h1>
      <h2>{t('Try again.')}</h2>
    </LostMessageRoot>
  );
};

export default LostMessage;
