export default `/*
* To win this level you need to collect all
* coins and get to the exit door and open it.
*
* Remember! Shorter code, bigger score.
* Comments are helpful in programming,
* therefore we won't count them in
* the score calculation.
*
* You can use these commands:
* # move();
* # turnLeft();
* # turnRight();
* # take();
* # use();
* # breakpoint();
*/
`;
