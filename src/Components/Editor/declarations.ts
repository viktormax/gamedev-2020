export default `
declare function move(): void;
declare function turnLeft(): void;
declare function turnRight(): void;
declare function take(): void;
declare function use(): void;
declare function breakpoint(): void;
`;
