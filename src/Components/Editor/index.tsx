import { useEffect, useMemo, useRef } from 'react';
import MonacoEditor, { Monaco, OnMount } from '@monaco-editor/react';
import useEvent from 'Hooks/useEvent';
import declarations from './declarations';
import defaultCode from './defaultCode';
import localAppStorage from 'Helpers/localAppStorage';
import Loader from 'Components/Loader';
import { useAppDispatch, useAppSelector } from 'Store';
import { setEditor } from 'Store/editorSlice';

interface Props {
  level: number;
  width: string;
}

const Editor: React.FC<Props> = ({ level, width }) => {
  const monacoRef = useRef<Monaco>();
  const editorRef = useRef<any>();
  const dispatch = useAppDispatch();
  const debug = useAppSelector((store) => store.game.debug);
  const initialCode = useMemo(() => localAppStorage.levelCode.get(level) ?? defaultCode, [level]);

  useEffect(() => {
    editorRef.current?.setValue(initialCode);

    const handleExit = () => {
      if (editorRef.current) {
        localAppStorage.levelCode.set(editorRef.current.getValue(), level);
      }
    };

    window.addEventListener('beforeunload', handleExit);

    return () => {
      handleExit();
      window.removeEventListener('beforeunload', handleExit);
    };
  }, [initialCode, level]);

  const onMount: OnMount = useEvent((editor, monaco) => {
    dispatch(setEditor(editor));
    monacoRef.current = monaco;
    editorRef.current = editor;
    monaco.languages.typescript.javascriptDefaults.setEagerModelSync(true);
    monaco.languages.typescript.javascriptDefaults.addExtraLib(declarations);
  });

  return (
    <MonacoEditor
      width={width}
      defaultLanguage="javascript"
      theme="vs-dark"
      defaultValue={initialCode}
      onMount={onMount}
      loading={<Loader />}
      options={{ readOnly: debug }}
    />
  );
};

export default Editor;
