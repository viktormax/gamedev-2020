import React from 'react';
import { useNavigate } from 'react-router-dom';
import Icon from 'Components/Icon';
import useEvent from 'Hooks/useEvent';
import { pathBuilder } from 'Helpers/utils';
import RoutePath from 'Enums/RoutePath';
import { MapBlocked, MapOpened, MapRoot } from './styles';
import map0 from './images/map0.png';
import map1 from './images/map1.png';
import map2 from './images/map2.png';
import map3 from './images/map3.png';
import map4 from './images/map4.png';
import { useTranslation } from 'react-i18next';

export const MAP_IMAGES = [map0, map1, map2, map3, map4];

interface Props {
  level: number;
  open: boolean;
}

const Map: React.FC<Props> = ({ level, open }) => {
  const navigate = useNavigate();
  const { t } = useTranslation();

  const onClick = useEvent(() => {
    navigate(pathBuilder(RoutePath.Game).set('level', level).get());
  });

  if (open) {
    return (
      <MapRoot onClick={onClick} $image={MAP_IMAGES[level]}>
        <MapOpened>
          {t('Level')} {level + 1}
        </MapOpened>
      </MapRoot>
    );
  }

  return (
    <MapRoot $image={MAP_IMAGES[level]}>
      <MapBlocked>
        <Icon icon="lock" size={48} />
      </MapBlocked>
    </MapRoot>
  );
};

export default Map;
