import styled from 'styled-components';

export const MapRoot = styled.div<{ $image: string }>`
  min-width: 260px;
  min-height: 170px;
  margin: 5vh;
  width: 20vw;
  height: 13vw;
  border-radius: 20px;
  text-decoration: none;
  color: white;
  transition: all 0.2s linear;
  font-family: Open-sans, sans-serif;
  overflow: hidden;
  background: url(${(p) => p.$image}) center no-repeat;
  background-size: cover;

  &:hover {
    color: gold;
  }
`;

export const MapBlocked = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgba(0, 0, 0, 0.7) center center no-repeat;
  cursor: not-allowed;
`;

export const MapOpened = styled.div`
  cursor: pointer;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2.5vw;
`;
