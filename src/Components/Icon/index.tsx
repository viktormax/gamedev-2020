import React, { memo } from 'react';
import icons from 'Icons';

export type IconName = keyof typeof icons;

interface Props {
  color?: string;
  icon: IconName;
  size?: string | number;
  width?: string | number;
  height?: string | number;
  margin?: string | number;
}

const Icon: React.FC<Props> = ({ icon, color, size = 32, height = size, width = size, margin }) => {
  const IconComponent = icons[icon];

  return (
    <IconComponent
      color={color}
      height={height}
      width={width}
      style={{ display: 'inline-block', fill: 'currentcolor', pointerEvents: 'none', margin }}
    />
  );
};

export default memo(Icon);
