import React from 'react';
import { CommandName, CommandRoot } from './styles';

interface Props {
  name: string;
  description: string;
  children?: React.ReactNode;
}

const Command: React.FC<Props> = ({ name, description, children }) => (
  <CommandRoot>
    <div>
      <CommandName>{name}</CommandName>
      <p>{description}</p>
    </div>
    <div>{children}</div>
  </CommandRoot>
);

export default Command;
