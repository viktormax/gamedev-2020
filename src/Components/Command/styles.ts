import styled from "styled-components";

export const CommandRoot = styled.div`
  margin-top: 5vh;
  text-align: center;
  min-width: 500px;
  width: 40%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const CommandName = styled.h1`
  color: #ffc50d;
  font-size: 2.5em;
  text-align: center;
  font-weight: 400;
  margin-bottom: 40px;
`;
