import styled from 'styled-components';

export const ItemRoot = styled.div`
  color: white;
  background: #444e5f;
  box-shadow: 4px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 1000px;
  display: flex;
  align-items: center;
  height: 42px;
`;

export const ImageWrapper = styled.div`
  height: 42px;
  width: 42px;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    height: 28px;
  }
`;

export const Text = styled.div`
  margin: 0;
  text-align: center;
  padding: 0 16px;
`;

export const Count = styled(Text)`
  padding-left: 8px;
`;
