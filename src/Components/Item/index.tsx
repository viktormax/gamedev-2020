import React, { memo } from 'react';
import key from './images/key.png';
import gold from './images/gold.png';
import { Count, ImageWrapper, ItemRoot, Text } from './styles';

const IMAGES = {
  key,
  gold,
};

interface Props {
  image?: keyof typeof IMAGES;
  text?: string;
  count?: number;
  total?: number;
}

const Item: React.FC<Props> = ({ image, count, total, text }) => {
  if (!image) {
    return (
      <ItemRoot>
        <Text>{text}</Text>
      </ItemRoot>
    );
  }

  return (
    <ItemRoot>
      <ImageWrapper>
        <img src={IMAGES[image]} alt={image} />
      </ImageWrapper>
      <Count>
        {count} {total != null ? `/ ${total}` : ''}
      </Count>
    </ItemRoot>
  );
};

export default memo(Item);
