import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const WinMessageRoot = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 99999;
  background: rgba(40, 44, 52, 0.6);
  position: absolute;
  flex-direction: column;

  h1 {
    margin: 20px;
    font-size: 4em;
    color: #ffc50d;
  }

  h2 {
    margin: 0;
    font-size: 1.6em;
    color: white;
  }
`;

export const ButtonLink = styled(Link)`
  margin-top: 90px;
  padding: 15px 20px;
  color: white;
  background: #1f8efa;
  border-radius: 8px;
  text-decoration: none;
`;
