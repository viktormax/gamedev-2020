import { useTranslation } from 'react-i18next';
import { ButtonLink, WinMessageRoot } from './styles';
import RoutePath from 'Enums/RoutePath';
import { useMemo } from 'react';
import { pathBuilder } from 'Helpers/utils';
import NyanCat from './nyan-cat.gif';

interface Props {
  score?: number;
  nextLevel?: number;
}

const WinMessage: React.FC<Props> = ({ score, nextLevel }) => {
  const { t } = useTranslation('common');

  const link = useMemo(
    () =>
      nextLevel ? pathBuilder(RoutePath.Game).set('level', nextLevel).get() : RoutePath.Levels,
    [nextLevel]
  );

  return (
    <WinMessageRoot>
      <img src={NyanCat} alt="nyan-cat" width={180} />
      <h1>{t('You Win!')}</h1>
      <h2>{t('Your score is {{score}}.', { score })}</h2>
      <ButtonLink to={link}>{nextLevel ? t('Next Level') : t('Go to Levels')}</ButtonLink>
    </WinMessageRoot>
  );
};

export default WinMessage;
