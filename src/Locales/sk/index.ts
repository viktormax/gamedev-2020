import about from './about.json';
import common from './common.json';
import layout from './layout.json';
import howToPlay from './howToPlay.json';
import prologue from './prologue.json';
import forBeginners from './forBeginners.json';

export default {
  about,
  common,
  layout,
  howToPlay,
  prologue,
  forBeginners,
};
