import Link from 'Components/Link';
import { AboutRoot, List } from './styles';
import { Title, SubTitle } from 'Theme/common';
import { useTranslation } from 'react-i18next';

const About = () => {
  const { t } = useTranslation('about');

  return (
    <AboutRoot>
      <div className="content">
        <Title>{t('About Game')}</Title>
        <p>
          {t(
            'This game was initially created as a team project for the subject "Game Development" at the Technical University of Košice in the academic year 2019/2020. Created according to topic no. 1: "Learn by playing games." Three students participated in the development of the game: Viktor Maksym (team leader), Pavel Grega, Denis Lukčo.'
          )}
        </p>
        <SubTitle>{t('Technologies and Frameworks')}</SubTitle>
        <List>
          <li>React</li>
          <li>TypeScript</li>
          <li>Phaser</li>
          <li>Styled Components</li>
          <li>Redux</li>
          <li>i18next</li>
          <li>PWA</li>
        </List>
        <SubTitle>{t('Resources')}</SubTitle>
        <p>{t('Icons and images:')}</p>
        <ul>
          <li>
            <Link href="https://www.flaticon.com/">https://www.flaticon.com</Link>
          </li>
          <li>
            <Link href="https://tenor.com/">https://tenor.com</Link>
          </li>
          <li>
            <Link href="https://icons8.com/">https://icons8.com</Link>
          </li>
          <li>
            <Link href="https://www.vecteezy.com/">https://www.vecteezy.com</Link>
          </li>
        </ul>
        <p>{t('Tileset')}:</p>
        <Link
          href="https://graphicriver.net/item/the-dungeon-top-down-tileset/21495485"
          margin="0 0 0 40px"
        >
          https://graphicriver.net/item/the-dungeon-top-down-tileset/21495485
        </Link>
      </div>
    </AboutRoot>
  );
};

export default About;
