import styled from 'styled-components';

export const AboutRoot = styled.div`
  background: radial-gradient(rgba(40, 44, 52, 0.8), rgba(40, 44, 52, 0.85));
  color: white;
  padding: 48px 10vw;
  min-height: 100%;
  box-sizing: border-box;

  p {
    font-size: 1.2em;
    line-height: 1.6em;
  }
`;

export const List = styled.div`
  font-size: 1.2em;
  margin-left: 10px;

  li + li {
    margin-top: 4px;
  }
`;
