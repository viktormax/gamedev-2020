import RoutePath from 'Enums/RoutePath';
import localAppStorage from 'Helpers/localAppStorage';
import { useAppSelector } from 'Store';
import HowToPlay from 'Pages/HowToPlay';
import { Navigate, Outlet, useParams } from 'react-router-dom';

const GameRouteFilter = () => {
  const params = useParams();
  const level = +(params.level ?? -1);
  const acknowledged = useAppSelector((store) => store.localStorage.acknowledged);
  const finishedLevels = localAppStorage.finishedLevels.get() ?? 0;

  if (level === -1 || level > finishedLevels) {
    return <Navigate to={RoutePath.Levels} />;
  }

  if (!acknowledged) {
    return <HowToPlay showConfirm />;
  }

  return <Outlet />;
};

export default GameRouteFilter;
