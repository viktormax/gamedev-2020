import { Row } from 'Theme/common';
import styled from 'styled-components';

export const GameRoot = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const FlexRow = styled(Row)`
  flex: 1;
`;
