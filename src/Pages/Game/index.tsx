import { useParams } from 'react-router-dom';
import Header from 'Components/Header';
import GameScene from 'Components/GameScene';
import Editor from 'Components/Editor';
import { FlexRow, GameRoot } from './styles';
import { useLayoutEffect } from 'react';
import { updateDebug } from 'Store/gameSlice';
import { useAppDispatch } from 'Store';

const Game = () => {
  const params = useParams();
  const level = +(params.level ?? -1);
  const dispatch = useAppDispatch();

  useLayoutEffect(() => {
    dispatch(updateDebug(params.debug === 'debug'));
  }, [dispatch, params]);

  return (
    <GameRoot>
      <Header level={level} />
      <FlexRow>
        <GameScene level={level} width="70vw" />
        <Editor level={level} width="30vw" />
      </FlexRow>
    </GameRoot>
  );
};

export default Game;
