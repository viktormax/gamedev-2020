import React from 'react';
import Item from 'Components/Item';
import move from './images/move.png';
import PlayerNoItem from './images/player-no-item.png';
import PlayerKey from './images/player-key.png';
import PlayerOpenChest from './images/player-open-chest.png';
import PlayerCloseChest from './images/player-close-chest.png';
import PlayerEmptyChest from './images/player-empty-chest.png';
import HashOpen from './images/hash-open.png';
import HashClose from './images/hash-close.png';
import { setAcknolaged } from 'Store/localStorageSlice';
import useEvent from 'Hooks/useEvent';
import { useAppDispatch } from 'Store';
import { Commands, Confirm, Description, HowToPlayRoot, ItemsRow } from './styles';
import { useTranslation } from 'react-i18next';
import Command from 'Components/Command';
import { Title, SubTitle } from 'Theme/common';

interface Props {
  showConfirm?: boolean;
}

const HowToPlay: React.FC<Props> = ({ showConfirm }) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation('howToPlay');

  const handleUnderstand = useEvent(() => {
    dispatch(setAcknolaged(true));
  });

  return (
    <HowToPlayRoot className="HowPlay">
      <Description>
        <Title>{t('Introduction')}</Title>
        <p>
          {t(
            "CodeOn is both a game and an educational tool for the basics of algorithms and programming. The main purpose of the game is to teach the player to think algorithmically and to practice his knowledge in an engaging and entertaining way. The game contains two timelines. The story takes place in the 17th century in the territory of today's Slovakia, but the player is invited to learn and practice the knowledge of contemporary algorithmization."
          )}
        </p>
        <SubTitle>{t('The course of the game')}</SubTitle>
        <p>
          {t(
            'To complete each level, the character must collect all the chests with coins and reach the exit. The player controls the character using JavaScript commands that he can write into the editor on the right side of the screen. There is a play button to run a sequence of written commands. If the player does not pass the level, he starts from the beginning. However, he can use the breakpoint() function to skip commands called before that function.'
          )}
        </p>
        <SubTitle>{t('Game Goal')}</SubTitle>
        <p>
          {t(
            "The player must think of a sequence of commands to complete the entire level. The challenge is to write code efficiently. The problem needs to be broken down into smaller parts and the player needs to look for actions that the character needs to perform repeatedly. The player must find a certain pattern of actions in each part of the level. This means that the player must learn how to write loops and custom functions. Each level has a sufficient number of commands that the player can call. When performing commands, the player can see the energy decrease at the top. The player has passed the level if the figure has completed all mandatory actions and still has energy. However, the score is calculated based on the number of characters in the code. The shorter the player's code, the better the result."
          )}
        </p>
      </Description>
      <Commands>
        <Command
          name="take()"
          description={t(
            'This command causes the character to take a coin from an open chest or key that he finds in the game world.'
          )}
        >
          <ItemsRow>
            <Item image="key" count={0} />
            <Item image="key" count={1} />
            <img src={PlayerKey} alt={t('Player and key.')} />
            <img src={PlayerNoItem} alt={t('Player and no items.')} />
          </ItemsRow>
          <ItemsRow>
            <Item image="gold" count={0} total={3} />
            <Item image="gold" count={1} total={3} />
            <img src={PlayerOpenChest} alt={t('Player with open chest.')} />
            <img src={PlayerEmptyChest} alt={t('Player with empty chest.')} />
          </ItemsRow>
        </Command>
        <Command
          name="use()"
          description={t(
            'This command causes the character to perform one of three actions. If the character has a key, he can open chests or barred doors. If the character stands on the control lever, he can open the door.'
          )}
        >
          <ItemsRow>
            <Item image="key" count={1} />
            <Item image="key" count={0} />
            <img src={PlayerCloseChest} alt={t('Player with close chest.')} />
            <img src={PlayerOpenChest} alt={t('Player with open chest.')} />
          </ItemsRow>
          <ItemsRow>
            <Item image="key" count={1} />
            <Item image="key" count={0} />
            <img src={HashClose} style={{ paddingTop: '15px' }} alt={t('Barred doors closed')} />
            <img src={HashOpen} style={{ paddingTop: '15px' }} alt={t('Barred doors opened')} />
          </ItemsRow>
        </Command>
        <Command
          name="breakpoint()"
          description={t(
            "This command is intended for a situation where a player has a good piece of code but doesn't want to look at the same execution again. He can use this command and the next time the player runs the code, the character will continue from that point."
          )}
        />
        <Command
          name="turnLeft() / turnRight()"
          description={t(
            'The player turns the character in the desired direction. The initial direction is always north.'
          )}
        />
        <Command
          name="move()"
          description={t(
            'This command causes the character to move to the next square in its direction.'
          )}
        >
          <img src={move} alt={t('Move animation.')} />
        </Command>
      </Commands>
      {showConfirm && <Confirm onClick={handleUnderstand}>{t('Understand')}</Confirm>}
    </HowToPlayRoot>
  );
};

export default HowToPlay;
