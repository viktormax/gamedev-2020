import styled from 'styled-components';

export const HowToPlayRoot = styled.div`
  background: radial-gradient(rgba(40, 44, 52, 0.8), rgba(40, 44, 52, 0.85));
  color: white;
  padding: 48px 0;
  font-size: 1.1em;
  line-height: 1.6em;
  min-height: 100%;
`;

export const Description = styled.div`
  padding: 0 10vw;

  h2 {
    text-align: center;
    padding: 0;
    margin: 30px 0 0 0 !important;
  }
`;

export const Commands = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;

export const Confirm = styled.button`
  cursor: pointer;
  transform: translateX(-50%);
  margin: 6vh 0 3vh 0;
  margin-left: 50%;
  font-size: 24px;
  background: #ffc50d;
  color: #282c34;
  border: none;
  border-radius: 1000px;
  padding: 30px 50px;
`;

export const ItemsRow = styled.div`
  margin-top: 1vh;
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-items: center;
`;
