import { FlexCenter } from 'Theme/common';
import styled from 'styled-components';

export const UnderReconstructionRoot = styled(FlexCenter)`
  height: 100%;
  flex-direction: column;
  color: white;
`;
