import React from 'react';
import { UnderReconstructionRoot } from './styles';
import Construction from './under-construction.png';

const UnderReconstruction: React.FC = () => (
  <UnderReconstructionRoot>
    <img src={Construction} alt="Under Construction" width={260} />
    <h1>This page is under reconstruction.</h1>
  </UnderReconstructionRoot>
);

export default UnderReconstruction;
