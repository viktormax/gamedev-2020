import styled from 'styled-components';

export const LevelsRoot = styled.div`
  background: radial-gradient(rgba(40, 44, 52, 0.3), rgba(40, 44, 52, 0.8));
  min-height: 100vh;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  align-content: center;
`;
