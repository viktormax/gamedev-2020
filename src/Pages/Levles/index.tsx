import Map, { MAP_IMAGES } from 'Components/Map';
import localAppStorage from 'Helpers/localAppStorage';
import { LevelsRoot } from './styles';
import { generateSequance } from 'Helpers/utils';

export const LEVELS = generateSequance(MAP_IMAGES.length);

const Levels = () => {
  const finishedLevels = localAppStorage.finishedLevels.get() ?? 0;

  return (
    <LevelsRoot>
      {LEVELS.map((level) => (
        <Map key={level} open={level <= finishedLevels} level={level} />
      ))}
    </LevelsRoot>
  );
};

export default Levels;
