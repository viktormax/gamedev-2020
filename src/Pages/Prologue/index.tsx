import { useTranslation } from 'react-i18next';
import { PrologueRoot } from './styles';
import { Title } from 'Theme/common';

const Prologue = () => {
  const { t } = useTranslation('prologue');

  return (
    <PrologueRoot>
      <Title>{t('Story')}</Title>
      <p>
        {t(
          'The year is 1619, a time marked by tumultuous political unrest within the Habsburg Empire. You stand as a seasoned soldier in the ranks of Gabriel Betlen, a prominent prince hailing from Transylvania. Amidst the currents of change sweeping through the empire, Betlen emerges as a significant figure leading a movement for reform and sovereignty.'
        )}
      </p>
      <p>
        {t(
          "As tensions escalate, you find yourself amidst the fervor of a significant movement, fighting alongside Betlen's forces, which swell to over 18,000 strong. In August, you march into the territory of eastern Slovakia, and by the dawn of September, the city of Košice falls under your command."
        )}
      </p>
      <p>
        {t(
          'However, amidst the preparations for a decisive confrontation, a treacherous plot is unveiled. A betrayer within the ranks seeks to poison the prince, but through your vigilance, the scheme is thwarted. Grateful for your loyalty, Prince Betlen offers you a boon, promising to grant any request.'
        )}
      </p>
      <p>
        {t(
          "With a heavy heart and the weight of uncertainty, you request time to deliberate on your desire. The following day, amidst the jubilation of Betlen's ascension to the throne as the King of Hungary, you approach him once more. Fueled by love and longing, you plead for the hand of his daughter, with whom you share a clandestine affection."
        )}
      </p>
      <p>
        {t(
          'However, your request is met with vehement opposition. The king had already arranged a strategic marriage between his daughter and the son of Frederick V, the powerful Elector Palatine of the Rhine, aimed at bolstering alliances and consolidating power against external threats.'
        )}
      </p>
      <p>
        {t(
          'In a fit of rage, Betlen condemns you to the depths of an underground prison within Cassovia Castle, scheduling your execution for the near future. With time ticking away and your life hanging in the balance, you must summon all your wit and courage to escape the clutches of imprisonment.'
        )}
      </p>
      <p>
        {t(
          'Locked within the confines of the gloomy dungeon, you embark on a daring quest for freedom, driven by the desire to reunite with your beloved princess. Evading guards and navigating treacherous obstacles, you plot your daring escape, fueled by determination and love.'
        )}
      </p>
      <p>
        {t(
          'Emerging into the moonlit night as a fugitive, you embark on a perilous journey to reclaim your destiny. Each step forward is fraught with danger, but with love as your compass and courage as your guiding light, you vow to defy all odds and seize the happiness that awaits you.'
        )}
      </p>
    </PrologueRoot>
  );
};

export default Prologue;
