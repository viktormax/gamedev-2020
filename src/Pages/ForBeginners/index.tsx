import { useTranslation } from 'react-i18next';
import { SubTitle, Title } from 'Theme/common';
import { CopyBlock, androidstudio } from 'react-code-blocks';
import { ForBeginnersRoot } from './styles';
import Link from 'Components/Link';

const ForBeginners = () => {
  const { t } = useTranslation('forBeginners');

  return (
    <ForBeginnersRoot>
      <div className="content">
        <Title>{t('Introduction to JavaScript')}</Title>
        <p>
          {t(
            "Welcome to the world of JavaScript! In this game, you control the character's actions by writing JavaScript code. Don't worry if you're new to programming – we'll start with the basics."
          )}
        </p>
        <SubTitle>{t('What is JavaScript?')}</SubTitle>
        <p>
          {t(
            "JavaScript, often abbreviated as JS, is a versatile programming language widely used for web development. It follows a syntax similar to languages like C, C++, Java, and C#. It's essential to note that JavaScript (JS) is not the same as Java."
          )}
        </p>
        <SubTitle>{t('Getting Started')}</SubTitle>
        <p>{t('To begin, familiarize yourself with a few fundamental concepts:')}</p>
        <h3>{t('Variables')}</h3>
        <p>
          {t(
            "Variables are containers for storing data values. You declare a variable using the `var`, `let` or `const` keyword followed by the variable's name. It's good practice to initialize variables with an initial value."
          )}
        </p>
        <CopyBlock
          text={'var steps = 5;\nlet keys = 2;\nconst energy = 100;'}
          language="js"
          theme={androidstudio}
          showLineNumbers
        />
        <h3>{t('Functions')}</h3>
        <p>
          {t(
            "Functions are blocks of reusable code. They are defined using the `function` keyword followed by the function's name. Parameters can be passed into functions as placeholders for values."
          )}
        </p>
        <CopyBlock
          text={'function functionName(parameters) {\n  // code goes here\n}'}
          language="js"
          theme={androidstudio}
          showLineNumbers
        />
        <h3>{t('Loops')}</h3>
        <p>
          {t(
            'Loops are used to execute a block of code repeatedly. Two common types of loops are `for` and `while`. They help avoid repeating code.'
          )}
        </p>
        <CopyBlock
          text={
            'for (i = 0; i < 5; i++) {  \n  // code to repeat\n}\n\nvar i = 0;\nwhile (i < 5) {\n  // code to repeat\n  i++;\n}'
          }
          language="js"
          theme={androidstudio}
          showLineNumbers
        />
        <h3>{t('Conditions')}</h3>
        <p>
          {t(
            'Conditional statements execute different actions based on different conditions. They are constructed using `if`, `else if`, and `else`.'
          )}
        </p>
        <CopyBlock
          text={
            'if (condition) {\n  // code if condition is true\n} else if (anotherCondition) {\n  // code if anotherCondition is true\n} else {\n  // code if all conditions are false\n}'
          }
          language="js"
          theme={androidstudio}
          showLineNumbers
        />
        <SubTitle>{t('Putting It All Together')}</SubTitle>
        <p>
          {t(
            "Let's look at a simple example where we define a function to repeat a move a certain number of times:"
          )}
        </p>
        <CopyBlock
          text={
            'function repeatMove(steps) {\n  for (i = 0; i < steps; i++) {\n    move();\n  }\n}'
          }
          language="js"
          theme={androidstudio}
          showLineNumbers
        />
        <p>{t('To use this function, you simply call it with the desired number of steps:')}</p>
        <CopyBlock
          text="repeatMove(5); // Moves the character five steps"
          language="js"
          theme={androidstudio}
          showLineNumbers
        />
        <p>
          {t(
            'Now, head over to the text editor on the right and start experimenting with JavaScript code to control your character in the game! Remember, practice makes perfect.'
          )}
        </p>
        <SubTitle>{t('Learn More Here')}</SubTitle>
        <ul>
          <li>
            <Link href="https://www.w3schools.com/js/default.asp">
              https://www.w3schools.com/js/default.asp
            </Link>
          </li>
          <li>
            <Link href="https://javascript.info/">https://javascript.info/</Link>
          </li>
          <li>
            <Link href="https://www.tutorialspoint.com/javascript/index.htm">
              https://www.tutorialspoint.com/javascript/index.htm
            </Link>
          </li>
          <li>
            <Link href="https://www.geeksforgeeks.org/javascript-tutorial/">
              https://www.geeksforgeeks.org/javascript-tutorial/
            </Link>
          </li>
        </ul>
      </div>
    </ForBeginnersRoot>
  );
};

export default ForBeginners;
