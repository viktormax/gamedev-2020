import styled from 'styled-components';

export const ForBeginnersRoot = styled.div`
  min-height: 100%;
  box-sizing: border-box;
  background: radial-gradient(rgba(40, 44, 52, 0.8), rgba(40, 44, 52, 0.85));
  color: #fff;
  padding: 40px 10vw;
  font-size: 1.1em;
  line-height: 1.6em;
  letter-spacing: 0.01em;
`;
