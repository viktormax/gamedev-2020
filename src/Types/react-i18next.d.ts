import { resources } from 'i18n';

declare module 'i18next' {
  interface CustomTypeOptions {
    returnNull: false;
    defaultNS: 'common';
    resources: (typeof resources)['en'];
  }
}
