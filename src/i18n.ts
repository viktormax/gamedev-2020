import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from 'Locales/en';
import sk from 'Locales/sk';

export const resources = {
  en,
  sk,
};

i18n.use(initReactI18next).init({
  resources,
  returnNull: false,
  defaultNS: 'common',
  lng: 'en',
  keySeparator: false,
  interpolation: {
    escapeValue: false, // react already safes from xss
  },
});

export default i18n;
export type TranslationResources = (typeof resources)['en'];
export type { TFunction } from 'i18next';
