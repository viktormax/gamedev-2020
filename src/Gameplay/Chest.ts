import Phaser from 'phaser';
import ObjectData from './ObjectData';
import TextureKey from 'Enums/TextureKey';
import { getProperty } from 'Helpers/utils';
import Player from './Player';
import { Takable, Usable } from './Interfaces';

export default class Chest extends Phaser.Physics.Matter.Sprite implements Takable, Usable {
  private opened: boolean = false;

  private coins: number = 1;

  constructor(scene: Phaser.Scene, data: ObjectData) {
    super(scene.matter.world, data.x, data.y, TextureKey.ChestOpened);

    this.opened = getProperty(data, 'opened', this.opened);
    this.coins = getProperty(data, 'coins', this.coins);

    this.setTexture(this.opened ? TextureKey.ChestOpened : TextureKey.ChestClosed);
    this.setCollisionCategory(0);

    this.scene.add.existing(this);
  }

  getCoins() {
    return this.coins;
  }

  canInteract(player: Player): boolean {
    return player.inRadius(this, true);
  }

  use(player: Player) {
    return this.open(player);
  }

  open(player: Player) {
    if (this.opened || !player.hasKey()) {
      return false;
    }

    player.takeKey();
    this.opened = true;
    this.setTexture(TextureKey.ChestOpened);

    return true;
  }

  take(player: Player) {
    if (!this.opened || this.coins < 1) {
      return false;
    }

    player.addCoins(this.coins);
    this.coins = 0;
    this.setTexture(TextureKey.ChestEmpty);

    return true;
  }
}
