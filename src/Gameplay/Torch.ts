import Phaser from 'phaser';
import ObjectData from './ObjectData';
import TextureKey from 'Enums/TextureKey';
import Scene from './Scene';

export default class Torch extends Phaser.GameObjects.Sprite {
  constructor(scene: Scene, data: ObjectData) {
    super(scene, data.x, data.y, TextureKey.TorchOn);

    this.play(TextureKey.TorchOn);
    this.anims.setProgress(Math.random());

    this.scene.add.existing(this);
  }
}
