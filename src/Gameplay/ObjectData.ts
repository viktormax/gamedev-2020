import Phaser from 'phaser';

interface Property {
  name: string;
  type: string;
  value: any;
}

interface ObjectData extends Phaser.Types.Tilemaps.TiledObject {
  id: number;
  gid: number;
  name: string;
  x: number;
  y: number;
  width: number;
  height: number;
  rotation: number;
  type: string;
  visible: boolean;
  properties: Property[];
}

export default ObjectData;
