import Phaser from 'phaser';
import ObjectData from './ObjectData';
import TextureKey from 'Enums/TextureKey';
import { getProperty } from 'Helpers/utils';

export default class Door extends Phaser.Physics.Matter.Sprite {
  private opened: boolean = false;

  private id: number | null = null;

  constructor(scene: Phaser.Scene, data: ObjectData) {
    super(scene.matter.world, data.x, data.y, TextureKey.DoorClosed);

    this.opened = getProperty(data, 'opened', this.opened);
    this.id = getProperty(data, 'id', this.id);

    this.setTexture(this.opened ? TextureKey.DoorOpened : TextureKey.DoorClosed);
    this.setCollisionCategory(this.opened ? 0 : 1);
    this.setStatic(true);

    this.scene.add.existing(this);
  }

  isOpened() {
    return this.opened;
  }

  open() {
    if (this.opened) {
      return false;
    }

    this.opened = true;
    this.setTexture(TextureKey.DoorOpened);
    this.setCollisionCategory(0);

    return true;
  }

  close() {
    if (!this.opened) {
      return false;
    }

    this.opened = false;
    this.setTexture(TextureKey.DoorClosed);
    this.setCollisionCategory(1);

    return true;
  }
}
