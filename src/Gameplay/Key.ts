import TextureKey from 'Enums/TextureKey';
import Phaser from 'phaser';
import Player from './Player';
import ObjectData from './ObjectData';
import { Takable } from './Interfaces';

export default class Key extends Phaser.Physics.Matter.Sprite implements Takable {
  constructor(scene: Phaser.Scene, data: ObjectData) {
    super(scene.matter.world, data.x, data.y, TextureKey.GoldKey);

    this.setCollisionCategory(0);

    this.scene.add.existing(this);
  }

  canInteract(player: Player): boolean {
    return player.inRadius(this, true);
  }

  take(player: Player) {
    player.addKey();
    this.destroy(true);

    return true;
  }
}
