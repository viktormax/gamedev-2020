import Phaser from 'phaser';
import TextureKey from 'Enums/TextureKey';
import ObjectData from './ObjectData';
import { getProperty } from 'Helpers/utils';
import { isDoor } from 'Helpers/typeGuards';
import { Usable } from './Interfaces';
import Player from './Player';
import Scene from './Scene';

export default class Lever extends Phaser.Physics.Matter.Sprite implements Usable {
  private isWin: boolean = false;

  private doorId: number | null = null;

  constructor(scene: Scene, data: ObjectData) {
    super(scene.matter.world, data.x, data.y, TextureKey.LeverLeft);

    this.isWin = getProperty(data, 'win', this.isWin);
    this.doorId = getProperty(data, 'id', this.doorId);

    this.setCollisionCategory(0);

    this.scene.add.existing(this);
  }

  canInteract(player: Player): boolean {
    return player.inRadius(this, true);
  }

  use(player: Player) {
    return this.toggle(player);
  }

  toggle(player: Player) {
    const door = this.scene.children.getAll('id', this.doorId).find(isDoor);
    if (!door) {
      return false;
    }

    const success = door.isOpened() ? door.close() : door.open();
    this.setTexture(door.isOpened() ? TextureKey.LeverRight : TextureKey.LeverLeft);

    const scene = this.scene as Scene;
    if (door.isOpened() && this.isWin && player.hasCoins(scene.totalCoins)) {
      player.win();
    }

    return success;
  }
}
