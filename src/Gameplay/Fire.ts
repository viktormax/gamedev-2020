import Phaser from 'phaser';
import ObjectData from './ObjectData';
import TextureKey from 'Enums/TextureKey';

export default class Fire extends Phaser.GameObjects.Sprite {
  constructor(scene: Phaser.Scene, data: ObjectData) {
    super(scene, data.x, data.y, TextureKey.Fire);

    this.play(TextureKey.Fire);
    this.anims.setProgress(Math.random());

    this.scene.add.existing(this);
  }
}
