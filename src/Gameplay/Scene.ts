import Phaser from 'phaser';
import Key from './Key';
import Player from './Player';
import Spikes from './Spikes';
import Chest from './Chest';
import Lever from './Lever';
import Door from './Door';
import BarredDoor from 'Gameplay/BarredDoor';
import TextureKey from 'Enums/TextureKey';
import MapLayerKey from 'Enums/MapLayerKey';
import ObjectData from './ObjectData';
import Torch from './Torch';
import Fire from './Fire';
import store from 'Store';
import { setTotalCoins, setLoading } from 'Store/gameSlice';

const SIZE_96 = {
  frameWidth: 48,
  frameHeight: 96,
} as const;

const SIZE_48 = {
  frameWidth: 48,
  frameHeight: 48,
} as const;

const constructors = new Map([
  ['player', Player],
  ['gold_key', Key],
  ['spikes', Spikes],
  ['chest', Chest],
  ['door', Door],
  ['lever', Lever],
  ['hash', BarredDoor],
  ['torch', Torch],
  ['fire', Fire],
]);

export default class Scene extends Phaser.Scene {
  private level: number;

  player: Player | undefined;

  code: string | undefined;

  totalCoins: number = 0;

  constructor(level: number) {
    super('game');

    this.level = level;
  }

  update() {
    this.player?.update();
    this.executeCode();
  }

  private preload() {
    store.dispatch(setLoading(true));

    this.load.setPath(`${process.env.PUBLIC_URL}/assets/`);

    this.load.tilemapTiledJSON('map', `levels/level${this.level}.json`);

    this.load.image(TextureKey.Tiles, 'tiles.png');
    this.load.image(TextureKey.GoldKey, 'objects/gold_key.png');
    this.load.image(TextureKey.ChestOpened, 'objects/chest_opened.png');
    this.load.image(TextureKey.ChestClosed, 'objects/chest_closed.png');
    this.load.image(TextureKey.DoorOpened, 'objects/door_opened.png');
    this.load.image(TextureKey.DoorClosed, 'objects/door_closed.png');
    this.load.image(TextureKey.HashOpened, 'objects/hash_opened.png');
    this.load.image(TextureKey.HashClosed, 'objects/hash_closed.png');
    this.load.image(TextureKey.ChestEmpty, 'objects/chest_empty.png');
    this.load.image(TextureKey.LeverLeft, 'objects/lever_left.png');
    this.load.image(TextureKey.LeverRight, 'objects/lever_right.png');
    this.load.image(TextureKey.Skeleton, 'objects/skeleton.png');
    this.load.image(TextureKey.Player, 'objects/player.png');
    this.load.image(TextureKey.Bone, 'objects/bone.png');

    this.load.spritesheet(TextureKey.Idle, 'animations/idle.png', SIZE_96);
    this.load.spritesheet(TextureKey.Run, 'animations/run.png', SIZE_96);
    this.load.spritesheet(TextureKey.TorchOn, 'animations/torch_on.png', SIZE_48);
    this.load.spritesheet(TextureKey.Fire, 'animations/fire.png', SIZE_96);
    this.load.spritesheet(TextureKey.Spikes, 'animations/spikes.png', SIZE_48);

    this.load.on('complete', () => {
      store.dispatch(setLoading(false));
    });
  }

  private create() {
    const map = this.add.tilemap('map');
    const tileset = map.addTilesetImage('tiles', TextureKey.Tiles)!;

    map.createLayer(MapLayerKey.Lower, tileset);

    const walls = map.createLayer(MapLayerKey.Middle, tileset)!;
    walls.setCollisionBetween(0, 179);
    this.matter.world.convertTilemapLayer(walls);

    this.addAnimation(TextureKey.TorchOn, 8);
    this.addAnimation(TextureKey.Idle, 8);
    this.addAnimation(TextureKey.Run, 8);
    this.addAnimation(TextureKey.Fire, 5);
    this.addAnimation(TextureKey.Spikes, 1);

    const objectsLayer = map.getObjectLayer(MapLayerKey.Objects)!;
    (objectsLayer.objects as ObjectData[]).forEach(this.loadObject.bind(this));

    this.totalCoins = this.children.getChildren()
      .map((x) => (x instanceof Chest ? x.getCoins() : 0))
      .reduce((acc, val) => acc + val);
    store.dispatch(setTotalCoins(this.totalCoins));

    map.createLayer(MapLayerKey.Upper, tileset);

    this.game.scale.resize(map.width * map.tileWidth, map.height * map.tileHeight);
  }

  private addAnimation(name: string, frameRate: number) {
    if (this.anims.exists(name)) {
      return;
    }

    this.anims.create({
      key: name,
      frames: this.anims.generateFrameNumbers(name),
      frameRate,
      repeat: -1,
    });
  }

  private loadObject(data: ObjectData) {
    // Fix origin
    data.x += data.width / 2;
    data.y -= data.height / 2;

    const Constructor = constructors.get(data.name);

    if (Constructor) {
      const object = new Constructor(this, data);

      if (object instanceof Player) {
        this.player = object;
      }
    } else {
      this.add.sprite(data.x, data.y, data.name);
    }
  }

  sheduleCode(code: string | undefined) {
    this.code = code;
  }

  private async executeCode() {
    const { player } = this;
    if (!this.code || !player) {
      return;
    }

    const move = () => player.addToActionQueue(player.move);
    const turnLeft = () => player.addToActionQueue(player.turnLeft);
    const turnRight = () => player.addToActionQueue(player.turnRight);
    const take = () => player.addToActionQueue(player.take);
    const use = () => player.addToActionQueue(player.use);
    const breakpoint = () => player.breakpoint();

    const commands = {
      names: ['move', 'turnLeft', 'turnRight', 'take', 'use', 'breakpoint'],
      funcs: [move, turnLeft, turnRight, take, use, breakpoint],
    };

    try {
      // eslint-disable-next-line no-new-func
      const userFunction = new Function(...commands.names, this.code);
      userFunction(...commands.funcs);
    } catch (err) {
      // eslint-disable-next-line no-alert
      alert(err);
    }

    this.code = undefined;
  }
}
