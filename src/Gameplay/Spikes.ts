import Phaser from 'phaser';
import ObjectData from './ObjectData';
import TextureKey from 'Enums/TextureKey';
import { Interactable } from './Interfaces';
import Player from './Player';

export default class Spikes extends Phaser.Physics.Matter.Sprite implements Interactable {
  constructor(scene: Phaser.Scene, data: ObjectData) {
    super(scene.matter.world, data.x, data.y, TextureKey.Spikes);

    this.play(TextureKey.Spikes);
    this.setCollisionCategory(0);
    this.anims.setProgress(Math.random());

    this.scene.add.existing(this);
  }

  canInteract(player: Player): boolean {
    return player.inRadius(this, true);
  }

  onInteract(player: Player) {
    player.kill();
  }
}
