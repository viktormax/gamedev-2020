import Player from './Player';

export interface Interactable {
  x: number;
  y: number;
  width: number;
  height: number;
  setTint: (tint: number | undefined) => void;
  canInteract(player: Player): boolean;
  onInteract?(player: Player): void;
}

export interface Takable extends Interactable {
  take(player: Player): boolean;
}

export interface Usable extends Interactable {
  use(player: Player): boolean;
}
