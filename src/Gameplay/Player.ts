import Phaser from 'phaser';
import ObjectData from './ObjectData';
import TextureKey from 'Enums/TextureKey';
import { getProperty } from 'Helpers/utils';
import { Interactable } from './Interfaces';
import Scene from './Scene';
import { isInteractable, isTakable, isUsable } from 'Helpers/typeGuards';
import store from 'Store';
import { initPlayer, setCoins, setEnergy, setGameState, setKeys } from 'Store/gameSlice';

export default class Player extends Phaser.Physics.Matter.Sprite {
  private readonly interactionRadius: number = 0;

  private readonly maxActionQueueLength = 5000;

  private direction: number = 0;

  private energy: number = 0;

  private keys: number = 0;

  private coins: number = 0;

  private debugCircle: Phaser.GameObjects.Graphics | undefined;

  private actionQueue: Function[] = [];

  private interactables: Interactable[] = [];

  private isActionInProcess = false;

  private isDestroyed = false;

  private hasBreakpoint = false;

  constructor(scene: Scene, data: ObjectData) {
    super(scene.matter.world, data.x, data.y, TextureKey.Player);

    this.energy = getProperty(data, 'energy', this.energy);
    this.direction = getProperty(data, 'direction', this.direction);
    this.interactionRadius = Math.sqrt((this.width / 2) ** 2 * 2);

    store.dispatch(initPlayer(this.energy));

    this.play(TextureKey.Idle);
    this.setOrigin(0.5, 0.8);

    this.scene.add.existing(this);

    this.on('destroy', () => {
      this.isDestroyed = true;
      this.actionQueue = [];
    });

    if (store.getState().game.debug) {
      this.debugCircle = this.scene.add.graphics();
      this.setupKeyboard();
    }
  }

  update() {
    this.debugCircle
      ?.clear()
      .lineStyle(2, 0xff0000)
      .strokeCircle(this.x, this.y, this.interactionRadius);

    if (!this.isActionInProcess) {
      if (this.actionQueue.length > 0) {
        this.executeActionFromQueue();
      } else {
        this.idle();
      }
    }
  }

  async executeActionFromQueue() {
    const action = this.actionQueue.shift();

    if (action && this.energy > 0) {
      this.isActionInProcess = true;

      const energyUsed = await action.bind(this)();
      if (energyUsed) {
        this.setEnergy(this.energy - 1);
      }

      this.isActionInProcess = false;

      this.executeActionFromQueue();
    }
  }

  private setupKeyboard() {
    const keys = this.scene.input!.keyboard!.addKeys('W,S,A,D,U,T') as any;

    const setNorth = () => this.setDirection(0);
    const setEast = () => this.setDirection(90);
    const setSouth = () => this.setDirection(180);
    const setWesth = () => this.setDirection(270);

    keys.W.on('down', () => {
      this.actionQueue.push(setNorth);
      this.actionQueue.push(this.move);
    });

    keys.D.on('down', () => {
      this.actionQueue.push(setEast);
      this.actionQueue.push(this.move);
    });

    keys.S.on('down', () => {
      this.actionQueue.push(setSouth);
      this.actionQueue.push(this.move);
    });

    keys.A.on('down', () => {
      this.actionQueue.push(setWesth);
      this.actionQueue.push(this.move);
    });

    keys.U.on('down', () => {
      this.actionQueue.push(this.use);
    });

    keys.T.on('down', () => {
      this.actionQueue.push(this.take);
    });
  }

  private computeInteractions() {
    this.interactables = this.scene.children.getChildren().filter((x) => {
      if (!isInteractable(x)) {
        return false;
      }

      const canInteract = x.canInteract(this);

      if (store.getState().game.debug) {
        x.setTint(canInteract ? 0xff0000 : undefined);
      }

      if (canInteract && x.onInteract) {
        x.onInteract(this);
      }

      return canInteract;
    }) as unknown as Interactable[];
  }

  kill() {
    this.actionQueue = [];
    store.dispatch(setGameState('lost'));
  }

  win() {
    this.actionQueue = [];
    store.dispatch(setGameState('win'));
  }

  setEnergy(energy: number) {
    this.energy = energy;
    store.dispatch(setEnergy(this.energy));
  }

  addCoins(amount: number) {
    this.coins += amount;
    store.dispatch(setCoins(this.coins));
  }

  hasCoins(amount: number) {
    return this.coins === amount;
  }

  addKey() {
    this.keys += 1;
    store.dispatch(setKeys(this.keys));
  }

  hasKey() {
    return this.keys > 0;
  }

  takeKey() {
    this.keys -= 1;
    store.dispatch(setKeys(this.keys));
  }

  idle() {
    this.play(TextureKey.Idle, true);
  }

  setDirection(direction: number) {
    if (this.direction === direction % 360) {
      return false;
    }

    this.direction = direction % 360;
    return true;
  }

  addToActionQueue(func: Function) {
    if (this.actionQueue.length >= this.maxActionQueueLength) {
      this.kill();
      throw new Error(
        'The action pipeline has reached its maximum length, which may indicate a potential infinite loop.'
      );
    }

    this.actionQueue.push(func);
  }

  breakpoint() {
    this.hasBreakpoint = true;
    this.actionQueue.push(() => {
      this.hasBreakpoint = false;
    });
  }

  turnLeft() {
    return this.setDirection(this.direction - 90);
  }

  turnRight() {
    return this.setDirection(this.direction + 90);
  }

  take() {
    this.interactables.forEach((x) => isTakable(x) && x.take(this));
    return true;
  }

  use() {
    this.interactables.forEach((x) => isUsable(x) && x.use(this));
    return true;
  }

  async move() {
    const dX = Math.round(Math.sin((this.direction * Math.PI) / 180));
    const dY = Math.round(Math.cos((this.direction * Math.PI) / 180));

    this.play(TextureKey.Run, true);

    // Mirror animation
    if (dX === -1) {
      this.flipX = true;
    } else if (dX === 1) {
      this.flipX = false;
    }

    const totalDistance = this.width;

    if (this.hasBreakpoint) {
      this.setPosition(this.x + totalDistance * dX, this.y - totalDistance * dY);
      this.computeInteractions();
      return true;
    }

    const totalDurationMs = 200;
    const stepSize = 4;

    const stepCount = totalDistance / stepSize;
    const stepDuration = totalDurationMs / stepCount;

    let remainingSteps = stepCount;

    const promise = new Promise<boolean>((resolve) => {
      const int = setInterval(() => {
        if (this.isDestroyed) {
          clearInterval(int);
          return;
        }

        if (remainingSteps) {
          this.setPosition(this.x + stepSize * dX, this.y - stepSize * dY);
        } else {
          clearInterval(int);
          this.computeInteractions();
          resolve(true);
        }

        remainingSteps -= 1;
      }, stepDuration);
    });

    return promise;
  }

  inRadius(object: Interactable, useCenterPoint: boolean) {
    const x = useCenterPoint
      ? object.x
      : Phaser.Math.Clamp(this.x, object.x - object.width / 2, object.x + object.width / 2);
    const y = useCenterPoint
      ? object.y
      : Phaser.Math.Clamp(this.y, object.y - object.height / 2, object.y + object.height / 2);

    const distance = Phaser.Math.Distance.Between(this.x, this.y, x, y);
    return distance < this.interactionRadius;
  }
}
