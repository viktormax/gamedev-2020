import Phaser from 'phaser';
import ObjectData from './ObjectData';
import TextureKey from 'Enums/TextureKey';
import { Usable } from './Interfaces';
import { getProperty } from 'Helpers/utils';
import Player from './Player';
import Scene from './Scene';

export default class BarredDoor extends Phaser.Physics.Matter.Sprite implements Usable {
  private opened: boolean = false;

  private isWin: boolean = false;

  constructor(scene: Phaser.Scene, data: ObjectData) {
    super(scene.matter.world, data.x, data.y, TextureKey.HashClosed);

    this.opened = getProperty(data, 'opened', this.opened);
    this.isWin = getProperty(data, 'win', this.isWin);

    this.setTexture(this.opened ? TextureKey.HashOpened : TextureKey.HashClosed);
    this.setCollisionCategory(this.opened ? 0 : 1);
    this.setStatic(true);

    this.scene.add.existing(this);
  }

  canInteract(player: Player): boolean {
    return player.inRadius(this, false);
  }

  use(player: Player) {
    return this.open(player);
  }

  open(player: Player) {
    if (this.opened || !player.hasKey()) {
      return false;
    }
    
    player.takeKey();

    this.setCollisionCategory(0);
    this.setTexture(TextureKey.HashOpened);
    this.opened = true;

    const scene = this.scene as Scene;
    if (this.isWin && player.hasCoins(scene.totalCoins)) {
      player.win();
    }

    return true;
  }
}
