import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
html,
body {
  padding: 0;
  margin: 0;
  background-color: #282c34;
  font-family: Roboto, 'Segoe UI', 'Helvetica Neue', Arial, 'Noto Sans', sans-serif;
}
`;
