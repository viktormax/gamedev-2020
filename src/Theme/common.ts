import styled from 'styled-components';

export const Row = styled.div<{ $gap?: string }>`
  display: flex;
  flex-direction: row;
  ${(p) => p.$gap && `gap: ${p.$gap};`}
`;

export const FlexCenter = styled.div`
  justify-content: center;
  align-items: center;
  display: flex;
`;

export const Title = styled.h1`
  margin: 0.5em;
  font-weight: normal;
  color: #ffc50d;
  text-align: center;
  font-weight: bold;
  margin-bottom: 50px;
  font-size: 2.5em;
`;

export const SubTitle = styled.h2`
  color: #ffc50d;
  font-size: 1.5em;
  text-align: center;
  margin-top: 30px;
  font-weight: 500;
`;