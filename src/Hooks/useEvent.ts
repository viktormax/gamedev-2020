import { useRef, useInsertionEffect, useCallback } from 'react';

// TODO: use hook from react when released.
const useEvent = <T extends Function>(callback: T): T => {
  const ref = useRef<T>(callback);

  useInsertionEffect(() => {
    ref.current = callback;
  }, [callback]);

  return useCallback((...args: any) => ref.current(...args), []) as unknown as T;
};

export default useEvent;
