/* eslint-disable camelcase */
/**
 *  This file is generated. Do not modify it, instead modify the template.
 */

import { ReactComponent as arrow } from './arrow.svg';
import { ReactComponent as lock } from './lock.svg';
import { ReactComponent as off } from './off.svg';
import { ReactComponent as play } from './play.svg';
import { ReactComponent as restart } from './restart.svg';
import { ReactComponent as stop } from './stop.svg';
import { ReactComponent as thunder } from './thunder.svg';

const icons = {
  arrow,
  lock,
  off,
  play,
  restart,
  stop,
  thunder,
};

export default icons;
