/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs').promises;
const ejs = require('ejs');

const ICONS_FOLDER = './src/Icons';
const TEMPLATE = './scripts/templates/icon-index.ejs';

const iconsFilter = (file) => file.split('.')[1] === 'svg';

const modelMapper = (file) => ({ path: file, name: file.split('.')[0] });

const main = async () => {
  console.log('Generating icons index...');

  const files = (await fs.readdir(ICONS_FOLDER)).filter(iconsFilter).map(modelMapper);

  fs.writeFile(`${ICONS_FOLDER}/index.ts`, await ejs.renderFile(TEMPLATE, { files }));
};

main();
